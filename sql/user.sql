CREATE TABLE Fraisphp.role (
    code VARCHAR(1) NOT NULL,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (code)
);

INSERT INTO Fraisphp.role  VALUES
('A', 'A'),
('E', 'E'),
('S', 'S');

--------------------------------------------------------------------
CREATE TABLE Fraisphp.user(
    id VARCHAR(200) NOT NULL,
    name VARCHAR(100) NOT NULL,
    pwd_hash VARCHAR(40) NOT NULL,
    role VARCHAR(1) NOT NULL, -- Roles : 'A' = Accountant, 'E' = Employee.
    accountant VARCHAR(200), -- Comptable associé au user si employé (NULL si le user est un comptable).
    PRIMARY KEY (id),
FOREIGN KEY (role) REFERENCES `role` (code)
);


INSERT INTO Fraisphp.user VALUES
('a1', 'Jean', 'f29bc91bbdab169fc0c0a326965953d11c7dff83', 'A', NULL),
('a2', 'Alain', 'b9f85daa6f83cf02ce5c31913d1f64d3f5c8fade', 'A', NULL),
('a3', 'Patrick', '252bc06763afb3b6c2a0802f7346700ab55f46f5', 'A', NULL),
('e1', 'Clara', '84896d3e067884621c0f54334b8d840949665844', 'E', 'a1'),
('e2', 'Amelie', 'dc1885915a8902bc29de6d834477a26d748b60b8', 'E', 'a2'),
('e3', 'Christophe', '0e647f28da2af6b6f4a51e217990dd1cd4f66124', 'E', 'a2');

-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------

CREATE TABLE Fraisphp.Statut (
    code VARCHAR(20) NOT NULL,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (code)
);

INSERT INTO Fraisphp.Statut VALUES
('', 'Non-traité'),
('R', 'Refusé'),
('V', 'Validé'),
('B', 'Remboursé');

///OK

---------------------------------------------------------------------------------------------
CREATE TABLE Fraisphp.FraisForfait(
    user_id VARCHAR(200) NOT NULL,
    `month` VARCHAR(7) NOT NULL, -- Format : << yyyy-mm >>.
    km INT NOT NULL DEFAULT 0,
    night INT NOT NULL DEFAULT 0,
    step INT NOT NULL DEFAULT 0,
    meal INT NOT NULL DEFAULT 0,
    statut VARCHAR(20), -- Etats: '' = Non-traité, 'R' = Refusé, 'V' = Validé, 'B' = Back/Remboursé

    PRIMARY KEY (user_id, `month`),
    FOREIGN KEY (user_id) REFERENCES `user` (id),
    FOREIGN KEY (statut) REFERENCES `Statut` (code)
   );



INSERT INTO Fraisphp.FraisForfait  VALUES

('e1', '2021-01', 1, 2, 3, 4, 'R'),
('e1', '2021-02', 1, 2, 3, 4, ''),
('e1', '2021-03', 1, 2, 3, 4, ''),
('e2', '2021-01', 1, 2, 3, 4, ''),
('e2', '2021-02', 1, 2, 3, 4, 'R'),
('e2', '2021-03', 1, 2, 3, 4, ''),
('e3', '2021-01', 1, 2, 3, 4, 'R'),
('e3', '2021-02', 1, 2, 3, 4, ''),
('e3', '2021-03', 1, 2, 3, 4, '');
-- -------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
CREATE TABLE Fraisphp.FraisHorsForfait(
    line SERIAL,
    user_id VARCHAR(200) NOT NULL,
    `month` VARCHAR(7) NOT NULL, -- Format : << yyyy-mm >>.
    description VARCHAR(200) NOT NULL DEFAULT '',
    amount INT NOT NULL DEFAULT 0,

    PRIMARY KEY (line, user_id, `month`)  
);


ALTER TABLE FraisHorsForfait 
ADD CONSTRAINT fk_client_numero FOREIGN KEY (user_id, `month`) REFERENCES FraisForfait (user_id, `month`);


INSERT INTO Fraisphp.FraisHorsForfait (user_id, `month`, description, amount) VALUES
('e1', '2021-01', 'Achat de piles', 1),
('e1', '2021-01', 'Roues crevées', 2),
('e1', '2021-01', 'Voyage en train', 3),
('e1', '2021-02', 'Some expenses e1.4', 4),
('e1', '2021-02', 'Some expenses e1.5', 5),
('e1', '2021-02', 'Some expenses e1.6', 6),
('e1', '2021-03', 'Some expenses e1.7', 7),
('e1', '2021-03', 'Some expenses e1.8', 8),
('e1', '2021-03', 'Some expenses e1.9', 9);