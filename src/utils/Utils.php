<?php


function getRequestInfo(){ // Cette fonction permet de récupérer les info essentielles et de les regrouper pour les utiliser plus facilement 
    $r = new stdClass();

    $rootUrl = getConfVal('rootUrl');

    $r->httpMethod = strtoupper($_SERVER["REQUEST_METHOD"]);
    $r->queryParams = $_REQUEST;

    $r->path = '';
    $r->explodedPath = [];
    if (isset($_SERVER["REQUEST_URI"])) {
        $uri = $_SERVER["REQUEST_URI"];
        $xUri = explode('?', $uri);
        $r->path = $xUri[0];
        $r->explodedPath = explode('/', $r->path);
        if ((count($r->explodedPath) > 0) && ($r->explodedPath[0] == '')) {
            array_shift($r->explodedPath);
        }
        if ((count($r->explodedPath) > 0) && ($r->explodedPath[0] == $rootUrl)) {
            array_shift($r->explodedPath);
            $r->path = implode('/', $r->explodedPath);
        }
    } else {
        return false;
    }

    $r = get_object_vars($r);

    return $r;
}

function isAUserConnected() { // Permet de vérifier qu'un utilisateur est connexcté 
    $token = getToken();
    return (($token != null) && ($token != ''));
}

function stop() {
    exit();
}

function getToken() { // va chercher un token
    $queryParams = $GLOBALS['queryParams'];

    if (isset($queryParams['token'])) {
        return $queryParams['token'];
    }

    return '';
}

function setBaseUrl($url) { // Permet de mettre à jour l'URL en phase avec la page affichée
    echo '<script>window.history.replaceState("", "", "' . $url . '");</script>';
}

function setUrlQueryParam($paramName, $paramValue) {
    $GLOBALS['queryParams'][$paramName] = $paramValue;
}

function nav( // Permet de naviguer vers une nouvelle page
    string $file,
    array $data = null,
    string $newBaseUrl = null,
    string $token = null
) {
    if (($newBaseUrl != null) || ($newBaseUrl != '')) {
        setBaseUrl($newBaseUrl);
    }

    if (($token != null) || ($token != '')) {
        setUrlQueryParam('token', $token);
    }

    $path =  __DIR__ . DIRECTORY_SEPARATOR . "../views" . DIRECTORY_SEPARATOR . $file;

    ob_start();

    if ($data != null) {
        extract($data);
    }

    include_once $path;

    $content = ob_get_contents();

    ob_clean();
    
    echo $content;
}

function pwdHash($pwd) { // Permet de hacher un mot de passe
    return sha1($pwd);
}

function generateToken($userId, $role) { // Genere un token après l'identificiation de l'user
    $token = base64_encode($userId . ' ' . $role);
    return str_replace('=', '', $token);
}

function decodeToken($token) { // Permet de s'assurer que tous les symnboles du token peuvent passer sur internet 
    return base64_decode($token . str_repeat('=', strlen($token) % 4));
}

function getConnectedUser($token = null) { //Permet de vérifier qu'un User est connecté et de retourner l'utilisateur si il est bien connecté
    if (($token == null) || ($token == '')) {
        $token = getToken();
    }

    if ($token == '') {
        return '';
    }

    return explode(' ', decodeToken($token))[0];
}

function getConnectedUserRole($token = null) { // Permet de récupérer le rôle de l'utilisateur connecté
    if (($token == null) || ($token == '')) {
        $token = getToken();
    }

    if ($token == '') {
        return '';
    }

    return explode(' ', decodeToken($token))[1];
}

function getConfVal($confValIdSpec){ // Permet d'aller chercher une valeur de config grâce au Singleton ConfigReader
    $section =  '';
    $key = $confValIdSpec;

    $xConfValIdSpec = explode('.', $confValIdSpec);

    if (count($xConfValIdSpec) == 2) {
        $section = array_shift($xConfValIdSpec);
        $key = array_shift($xConfValIdSpec);
    }

    if ($section != '') {
        return ConfigReader::getInstance()->getValue($key, $section);
    } else {
        return ConfigReader::getInstance()->getValue($key);
    }
}

function getPreviousMonth() { // Permet d'obtenir le mois précédent 
    $today = getdate();
    $year = $today["year"];
    $month = $today["mon"];
    $month--;
    //$month--;
    if ($month == 0) {
        $month = 12;
        $year--;
    }
    $month = str_pad(strval($month), 2, '0', STR_PAD_LEFT);

    return $year . '-' . $month; 
}
function popup($text) {
    echo "<script>alert('" . $text . "')</script>";
}

?>
