<?php

require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/ConfigReader.php';

class DataBase { //Singleton
    public $cnx;
    
    private static $srv;
    private static $db;
    
    private static $usr;
    private static $pwd;
    
    private static $instance = null; 

    private function __construct() {
        self::$srv = ConfigReader::getInstance()->getValue("srv", "examenphp");
        self::$db = ConfigReader::getInstance()->getValue("db", "examenphp");
        self::$usr = ConfigReader::getInstance()->getValue("usr", "examenphp");
        self::$pwd = ConfigReader::getInstance()->getValue("pwd", "examenphp");
        
        $this->cnx = new PDO("mysql:host=" . self::$srv . ";dbname=" . self::$db, self::$usr, self::$pwd);
    }
    
    public static function getInstance() : DataBase {
        if(self::$instance == null) {
            self::$instance = new DataBase();
        }

        return self::$instance;
    }
}
