<?php

require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Utils.php';

class ConfigReader { // Singleton.
    private $config;
    private static $instance=null; 

    private function __construct() {
        $this->config = parse_ini_file('../conf.ini', true);
    }
    
    public static function getInstance() : ConfigReader
    {
        if(self::$instance == null) {
            self::$instance = new ConfigReader();
        }
        return self::$instance;
    }
    
    function getValue(string $key, string $section = null): ?string {
        if ($section != null) {
            if (array_key_exists($section, $this->config)) {
                if (array_key_exists($key, $this->config[$section])) {
                    return $this->config[$section][$key];
                } else {
                    return '';
                }
            } else {
                return '';
            }
        } else {
            if (array_key_exists($key, $this->config)) {
                return $this->config[$key];
            } else {
                return '';
            }
        }
    }
}
