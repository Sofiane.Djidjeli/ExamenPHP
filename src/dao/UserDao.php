<?php

require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Utils.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/model/User.php';

class UserDao {
    public $cnx;
    
    public function __construct(PDO $cnx) {
        $this->cnx = $cnx;
    }
    
    public function findById($userId) {
        $sql = 'SELECT * FROM user WHERE id = :userId';

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('userId', $userId);

        $preparedStatement->execute();

        $user = new User($preparedStatement->fetchObject('user'));

        if ($user->getId() == '') {
            return null;
        }

        return $user;
    }
    
    public function check($userId, $pwdHash) {
        $sql = 'SELECT * FROM user WHERE id = :userId AND pwd_hash = :pwdHash';

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('userId', $userId);
        $preparedStatement->bindValue('pwdHash', $pwdHash);

        $preparedStatement->execute();

        $u = new User($preparedStatement->fetchObject('user'));

        if ($u->getId() == '') {
            return false;
        }

        return true;
    }

    public function findAllEmployeesAssociatedToAccountant($accountantId) {
        $sql = "SELECT * FROM user WHERE accountant = :accountantId AND role = 'E' ORDER BY id";

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('accountantId', $accountantId);

        $preparedStatement->execute();

        $employees = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);

        return $employees;
    }
}