<?php

require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Utils.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/model/Expense.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/dao/UserDao.php';

class ExpenseDao {
    public $cnx;
    public $userDao;

    public function __construct(PDO $cnx) {
        $this->cnx = $cnx;
        $this->userDao = new UserDao($cnx);
    }
    
    public function findByPK($userId, $month) {
        $sql = 'SELECT * FROM FraisForfait WHERE user_id = :userId AND `month` = :month';

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('userId', $userId);
        $preparedStatement->bindValue('month', $month);

        $preparedStatement->execute();

        $expense = new Expense($preparedStatement->fetchObject('expense'));

        if ($expense->getUserId() == '') {
            return null;
        }

        return $expense;
    }
    
        
    public function createExpense($userId, $month) {
        $sql = "Insert into FraisForfait Values( :userId ,:month,0,0,0,0,'')";

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('userId', $userId);
        $preparedStatement->bindValue('month', $month);

        $preparedStatement->execute();

        return $this->findByPK($user, $month);

     
    }
    
    public function updateExpense($userId, $month) {
        $sql = "UPDATE FraisForfait SET km= :km , night= :night , step= :step , meal= :meal WHERE user_id = :userId AND `month` = :month";

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('userId', $userId);
        $preparedStatement->bindValue('month', $month);
        $preparedStatement->bindValue('km', $_POST['km']);
        $preparedStatement->bindValue('night', $_POST['night']);
        $preparedStatement->bindValue('step', $_POST['step']);
        $preparedStatement->bindValue('meal', $_POST['meal']);

        $preparedStatement->execute();
     
    }
        public function createExpenseLine($userId, $month) {
        $sql = "INSERT INTO FraisHorsForfait (user_id,month,description,amount) Values (:userId,:month,:description,:amount)";

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('userId', $userId);
        $preparedStatement->bindValue('month', $month);
        $preparedStatement->bindValue('description', $_POST['description']);
        $preparedStatement->bindValue('amount', $_POST['amount']);
    

        $preparedStatement->execute();
     
    }
       public function removeExpenseLine($line) {
        $sql = "DELETE FROM FraisHorsForfait where line=:line";

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('line', $line);

        $preparedStatement->execute();
    
    
    }
       public function updateExpenseStatus ($targetUser,$month,$status) {
        $sql = "UPDATE FraisForfait SET status=:status WHERE user_id = :userId AND `month` = :month";

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('userId', $targetUser);
        $preparedStatement->bindValue('month', $month);
        $preparedStatement->bindValue('status', $status);

        $preparedStatement->execute();        
        
    }
    
    
    
    public function findLinesByPK($userId, $month) {
        $sql = 'SELECT * FROM FraisHorsForfait WHERE user_id = :userId AND `month` = :month ORDER BY line';

        $preparedStatement = $this->cnx->prepare($sql);

        $preparedStatement->bindValue('userId', $userId);
        $preparedStatement->bindValue('month', $month);

        $preparedStatement->execute();

        $expenseLines = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);

        return $expenseLines;
    }

    public function findAllUprocessedExpensesForAccountant($accountantId) {
        $expenses = [];

        $associatedEmployees = $this->userDao->findAllEmployeesAssociatedToAccountant($accountantId);
        if (count($associatedEmployees) == 0) {
            return $expenses;
        }

        foreach($associatedEmployees as $employee) {
            $userId = $employee['id'];

            $sql = "SELECT * ,(SELECT name FROM `user` u where u.id= f.user_id) AS userName "
                    . "FROM FraisForfait f LEFT OUTER JOIN Statut s ON s.code = f.status "
                    . "WHERE user_id = :userId ORDER BY user_id, `month`";

            $preparedStatement = $this->cnx->prepare($sql);

            $preparedStatement->bindValue('userId', $userId);

            $preparedStatement->execute();

            $expensesForEmployee = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);
            foreach($expensesForEmployee as $expense) {
                array_push($expenses, $expense);
            }
        }

        return $expenses;
    }
}
