<?php

require_once '../src/utils/Utils.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/controllers/UserController.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/controllers/ExpenseController.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/controllers/ExpenseProcessingController.php';

class DefaultController {
    public function show($token = null) {
        if ((! isAUserConnected()) && ($token == null)) {
            (new UserController())->showLogin();
        } else {
            if (getConnectedUserRole($token) == 'E') {
                (new ExpenseController())->show($token);
            } else {
                (new ExpenseProcessingController())->show($token);
            }
        }
    }
}
