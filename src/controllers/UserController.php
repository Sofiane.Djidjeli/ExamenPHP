<?php

require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Utils.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Database.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/dao/UserDao.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/controllers/DefaultController.php';

class UserController {
    public $userDao;
    
    public function __construct() {
        $this->userDao = new UserDao(DataBase::getInstance()->cnx);
    }
    
    public function showLogin() {
        $token = getToken();
        $rootUrl = getConfVal('rootUrl');
        $publichtmlRootUrl = getConfVal('publichtmlRootUrl');

        nav(
            "login.php",
            compact('rootUrl', 'publichtmlRootUrl'),
            'user-login'
        );
    }

    public function login() {
        if (isset($_POST['userId'])) {
            $userId = htmlspecialchars($_POST['userId']);
        }

        if (isset($_POST['pwd'])) {
            $pwd = htmlspecialchars($_POST['pwd']);
            $pwdHash = pwdHash($pwd);
        }

        $user = $this->userDao->findById($userId);

        if ($user != null) {
            // Check password.
            if (! $this->userDao->check($userId, $pwdHash)) {
                popup('Wrong password for user "' . $user->getId() . '"');
                (new DefaultController())->show();
                return;
            }

            // Generate token.
            $token = generateToken($user->getId(), $user->getRole());

            (new DefaultController())->show($token);
        } else {
            popup('Unrecognized user "' . $userId . '"');
            (new DefaultController())->show();
        }
    }
}
