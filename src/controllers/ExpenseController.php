<?php

require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Utils.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Database.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/dao/ExpenseDao.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/dao/UserDao.php';

class ExpenseController {
    public $expenseDao;
    public $userDao;
    
    public function __construct() {
        $this->expenseDao = new ExpenseDao(DataBase::getInstance()->cnx);
        $this->userDao = new UserDao(DataBase::getInstance()->cnx);
    }
    
    public function show($token = null) {
        if (($token == null) || ($token == '')) {
            $token = getToken();
        }

        $rootUrl = getConfVal('rootUrl');
        $publichtmlRootUrl = getConfVal('publichtmlRootUrl');
        $user = getConnectedUser($token);
        if ($user == null) {
            stop();
        }
        $role = getConnectedUserRole($token);
        $u = $this->userDao->findById($user);
        $userName = $u->getName();
        $previousMonth = getPreviousMonth();
        $expense = $this->expenseDao->findByPK($user, $previousMonth);
        if ($expense==null){
            $expense = $this->expenseDao->createExpense($user, $previousMonth);
        }
        $expenseLines = $this->expenseDao->findLinesByPK($user, $previousMonth);

        nav(
            "expense.php",
            compact(
                'token', 'rootUrl', 'publichtmlRootUrl', 'user', 'role',
                'previousMonth', 'expense', 'expenseLines', 'userName'
            ),
            'expense-show?token=' . $token,
            $token
        );
    }
    public function update($token = null) {
       
       if (($token == null) || ($token == '')) {
            $token = getToken();
        }
        $user = getConnectedUser($token);
        if ($user == null) {
            stop();
        }
        $previousMonth = getPreviousMonth();

         $this->expenseDao->updateExpense($user, $previousMonth);

        $this->show($token);
    }
    
    public function createLine($token = null) {   
       if (($token == null) || ($token == '')) {
            $token = getToken();
        }
        $user = getConnectedUser($token);
        if ($user == null) {
            stop();
        }
        $previousMonth = getPreviousMonth();

         $this->expenseDao->createExpenseLine($user, $previousMonth);

        $this->show($token);
    
}
    public function removeLine($token = null) {   
       if (($token == null) || ($token == '')) {
            $token = getToken();
        }
        $user = getConnectedUser($token);
        if ($user == null) {
            stop();
        }
        $line = $GLOBALS['queryParams']['line'];
       
     
         $this->expenseDao->removeExpenseLine($line);

        $this->show($token);
    }
    
    
}
