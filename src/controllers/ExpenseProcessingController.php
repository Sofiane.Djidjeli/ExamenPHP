<?php

require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Utils.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Database.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/dao/ExpenseDao.php';
require_once '/home/student/Desktop/Examen/ExamenPHP/src/dao/UserDao.php';

class ExpenseProcessingController {
    public $expenseDao;
    public $userDao;
    
    public function __construct() {
        $this->expenseDao = new ExpenseDao(DataBase::getInstance()->cnx);
        $this->userDao = new UserDao(DataBase::getInstance()->cnx);
    }
    
    public function show($token = null) {
        if (($token == null) || ($token == '')) {
            $token = getToken();
        }

        $rootUrl = getConfVal('rootUrl');
        $publichtmlRootUrl = getConfVal('publichtmlRootUrl');
        $user = getConnectedUser($token);
        if ($user == null) {
            stop();
        }
        $role = getConnectedUserRole($token);
        $u = $this->userDao->findById($user);
        $userName = $u->getName();
        $previousMonth = getPreviousMonth();
        $expense = $this->expenseDao->findByPK($user, $previousMonth);
        $unprocessedExpenses = $this->expenseDao->findAllUprocessedExpensesForAccountant($user);


        nav(
            "expenseProcessing.php",
            compact(
                'token', 'rootUrl', 'publichtmlRootUrl', 'user', 'role',
                'previousMonth', 'expense', 'userName', 'unprocessedExpenses'
            ),
            'expenseProcessing-show?token=' . $token,
            $token
        );
    }
     public function approveExpense($token = null) {   
       if (($token == null) || ($token == '')) {
            $token = getToken();
        }
        $user = getConnectedUser($token);
        if ($user == null) {
            stop();
        }
        $targetUser = $GLOBALS['queryParams']['user'];
        $month = $GLOBALS['queryParams']['month'];

         $this->expenseDao->updateExpenseStatus($targetUser,$month,'V');

        $this->show($token);

}
   public function payExpense($token = null) {   
       if (($token == null) || ($token == '')) {
            $token = getToken();
        }
        $user = getConnectedUser($token);
        if ($user == null) {
            stop();
        }
        $targetUser = $GLOBALS['queryParams']['user'];
        $month = $GLOBALS['queryParams']['month'];

         $this->expenseDao->updateExpenseStatus($targetUser,$month,'B');

        $this->show($token);
}
  public function refuseExpense($token = null) {   
       if (($token == null) || ($token == '')) {
            $token = getToken();
        }
        $user = getConnectedUser($token);
        if ($user == null) {
            stop();
        }
        $targetUser = $GLOBALS['queryParams']['user'];
        $month = $GLOBALS['queryParams']['month'];

         $this->expenseDao->updateExpenseStatus($targetUser,$month,'R');

        $this->show($token);
}
}
