<?php

require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Utils.php';

class User {
    protected $id;
    protected $name;
    protected $pwdHash;
    protected $role;
    protected $accountant;

    public function __construct($data = null) {
        if (gettype($data) == 'object') {
            $this->id = $data->id;
            $this->name = $data->name;
            $this->pwdHash = $data->pwd_hash;
            $this->role = $data->role;
            $this->accountant = $data->accountant;
        }
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getPwdHash() {
        return $this->pwdHash;
    }

    function getRole() {
        return $this->role;
    }

    function getAccountant() {
        return $this->accountant;
    }
}
