<?php

require_once '/home/student/Desktop/Examen/ExamenPHP/src/utils/Utils.php';

class Expense {
    protected $userId;
    protected $month;
    protected $km;
    protected $night;
    protected $step;
    protected $meal;
    protected $status;

    public function __construct($data = null) {
        if (gettype($data) == 'object') {
            $this->userId = $data->user_id;
            $this->month = $data->month;
            $this->km = $data->km;
            $this->night = $data->night;
            $this->step = $data->step;
            $this->meal = $data->meal;
            $this->status = $data->status;
        }
    }

    function getUserId() {
        return $this->userId;
    }

    function getMonth() {
        return $this->month;
    }

    function getKm() {
        return $this->km;
    }

    function getNight() {
        return $this->night;
    }

    function getStep() {
        return $this->step;
    }

    function getMeal() {
        return $this->meal;
    }

    function getStatus() {
        return $this->status;
    }
}
