<!DOCTYPE html>
<html>
    <head>
        <title>Examen PHP</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <link href="<?php echo $publichtmlRootUrl;?>/css/views.css" rel="stylesheet" type="text/css"/>
        <!-- <?php include('styles.html'); ?> -->
    </head>
    <body>
        <?php include('nav_bar.html'); ?>

        <table _border="1" width="100%">
            <tr height="200px">
                <td rowspan="2" width="60%" style="padding:0; vertical-align:top;">
                    <div width="100%" style="background-color:lightblue; margin:10px; padding:1px; border-radius:10px;">
                        <div width="100%" style="_background-color:red; margin:5px; text-align:center; font-size:2em;">
                            <b><u>Fiche de frais</u></b>
                        </div>
                        <br/>
                        <div width="100%" style="_background-color:red; margin:5px; text-align:center;">
                            <table _border="1" width="100%">
                                <tr>
                                    <td style="text-align:left;font-size:1.2em;" width="50%">
                                        <b>Employé : <?php echo $userName ?> (<?php echo $user ?>)</b>
                                    </td>
                                    <td style="text-align:right;font-size:1.2em;">
                                        <b>Mois : <?php echo $previousMonth; ?></b>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <div width="100%" style="_background-color:red; margin:5px; text-align:center; font-size:1.5em;">
                            <b>Lignes non-forfaitisées</b>
                        </div>
                        <div width="100%" style="background-color:white; margin:5px; text-align:center; border-radius:20px;">
                            <table _border="1" width="100%">
                                <tr>
                                    <th style="text-align:center;" width="50%">
                                        <b><u>Description</u></b>
                                    </th>
                                    <th style="text-align:center;">
                                        <b><u>Montant</u></b>
                                    </th>
                                </tr>
                                <?php $i = 0; foreach($expenseLines as $expenseLine) : $i++ ?>
                                    <tr>
                                        <td style="text-align:center;" width="50%">
                                            <?php echo $expenseLine['description']; ?><br/>
                                        </td>
                                        <td style="text-align:center;">
                                            <?php echo $expenseLine['amount']; ?> &euro;<br/>
                                        </td>
                                        <td style="text-align:left;" width="30px">
                                            <a href="<?php echo $rootUrl;?>/expense_line-remove?line=<?php echo $expenseLine['line']; ?>&amp;token=<?php echo $token; ?>"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pen" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <br><br><br><br><div style="font-size:20px;">&nbsp;</div>
                    </div>
                </td>
                <td style="padding:0; vertical-align:top;">
                    <div width="100%" style="background-color:lightblue; margin:10px; padding:1px; border-radius:10px;">
                        <div width="100%" style="_background-color:red; margin:5px; text-align:center; font-size:2em;">
                            <b><u>Frais forfaitisés</u></b>
                        </div>
                        <form action="<?php echo $rootUrl;?>/expense-update?month=<?php echo $previousMonth; ?>&amp;token=<?php echo $token; ?>" method="POST">
                            <table _border="1" width="100%">
                                <tr align="left">
                                    <td style="padding-top:5px; padding-bottom:5px; padding-left:15%;">
                                        Km : <input name="km" type="number" min="0" max="1000" step="1" value="<?php echo $expense->getKm(); ?>">
                                    </td>
                                    <td style="padding-top:5px; padding-bottom:5px; padding-left:15%;">
                                        Nuit : <input name="night" type="number" min="0" max="1000" step="1" value="<?php echo $expense->getNight(); ?>">
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td style="padding-top:5px; padding-bottom:5px; padding-left:15%;">
                                        Etape : <input name="step"  type="number" min="0" max="1000" step="1" value="<?php echo $expense->getStep(); ?>">
                                    </td>
                                    <td style="padding-top:5px; padding-bottom:5px; padding-left:15%;">
                                        Repas : <input name="meal" type="number" min="0" max="1000" step="1" value="<?php echo $expense->getMeal(); ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top:15px; padding-bottom:10px;">
                                        <button type="submit" style="border-radius:10px;" >Mettre à jour</button>

                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding:0; vertical-align:top;">
                    <div width="100%" style="background-color:lightblue; margin:10px; padding:1px; border-radius:10px;">
                        <div width="100%" style="_background-color:red; margin:5px; text-align:center; font-size:2em;">
                            <b><u>Frais non-forfaitisés</u></b>
                        </div>
                        <form action="<?php echo $rootUrl;?>/expense_line-create?month=<?php echo $previousMonth; ?>&amp;token=<?php echo $token; ?>" method="POST">
                            <table _border="1" width="100%">
                                <tr align="left">
                                    <td style="padding-top:5px; padding-bottom:5px; padding-left:15%;">
                                        Description : <input name="description" type="text">
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td style="padding-top:5px; padding-bottom:5px; padding-left:15%;">
                                        Montant (&euro;): <input name="amount" type="number" min="1" max="1000" step="1">
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td colspan="2" style="padding-top:15px; padding-bottom:10px;">
                                        <button type="submit" style="border-radius:10px;" >Creer</button>
                                    </td>
                                </tr>
                            </table>
                        </form> 
                    </div>
                </td>
            </tr>
        </table>

    </body>
</html>
