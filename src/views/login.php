<!DOCTYPE html>
<html>
    <head>
        <title>Examen PHP</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <link href="<?php echo $publichtmlRootUrl;?>/css/views.css" rel="stylesheet" type="text/css"/>
        <!-- <?php include('styles.html'); ?> -->
    </head>
    <body>
        <?php include('nav_bar.html'); ?>

        <form action="<?php echo $rootUrl;?>/user-login" method="POST">
            <div align='center'> 
                <div class="col-sm-5 my-1">
                    <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">User</div>
                        </div>
                        <input type="text" class="form-control" name="userId" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 my-1">
                        <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Password</div>
                            </div>
                            <input type="text" class="form-control" name="pwd">
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 my-1">
                    <button type="submit" class="btn btn-primary" data-toggle="collapse">Valider</button>
                </div>
            </div>
        </form>
    </body>
</html>
