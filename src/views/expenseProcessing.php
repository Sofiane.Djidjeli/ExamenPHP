<!DOCTYPE html>
<html>
    <head>
        <title>Examen PHP</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <link href="<?php echo $publichtmlRootUrl;?>/css/views.css" rel="stylesheet" type="text/css"/>
        <!-- <?php include('styles.html'); ?> -->
    </head>
    <body>
        <?php include('nav_bar.html'); ?>

        <div width="100%" style="background-color:lightblue; margin:10px; padding:1px; border-radius:10px;">
            <div width="100%" style="_background-color:red; margin:5px; text-align:center; font-size:2em;">
                <b><u>Fiches de frais</u></b>
            </div>
            <br/>
            <div width="100%" style="background-color:white; margin:5px; text-align:center; border-radius:20px;">
                <table _border="1" width="100%">
                    <tr>
                        <th style="text-align:center;" width="15%">
                            <b><u>Employé</u></b>
                        </th>
                        <th style="text-align:center;" width="15%">
                            <b><u>Mois</u></b>
                        </th>
                        <th style="text-align:center;" width="15%">
                            <b><u>Km</u></b>
                        </th>
                        <th style="text-align:center;" width="15%">
                            <b><u>Nuit</u></b>
                        </th>
                        <th style="text-align:center;" width="15%">
                            <b><u>Etape</u></b>
                        </th>
                        <th style="text-align:center;" width="15%">
                            <b><u>Repas</u></b>
                        </th>
                        <th style="text-align:center;">
                            <b><u>Statut</u></b>
                        </th>
                    </tr>
                    <?php $i = 0; foreach($unprocessedExpenses as $expense) : $i++ ?>
                        <tr>
                            <td style="text-align:center;" width="15%">
                                <?php echo $expense['userName']; ?><br/>
                            </td>
                            <td style="text-align:center;" width="15%">
                                <?php echo $expense['month']; ?><br/>
                            </td>
                            <td style="text-align:center;" width="15%">
                                <?php echo $expense['km']; ?><br/>
                            </td>
                            <td style="text-align:center;" width="15%">
                                <?php echo $expense['night']; ?><br/>
                            </td>
                            <td style="text-align:center;" width="15%">
                                <?php echo $expense['step']; ?><br/>
                            </td>
                            <td style="text-align:center;" width="15%">
                                <?php echo $expense['meal']; ?><br/>
                            </td>
                            <td style="text-align:center;">
                                <?php echo $expense['name']; ?><br/>
                            </td>
                            <td style="text-align:left;" width="70px">
                                <a href="<?php echo $rootUrl;?>/expense-approve?user=<?php echo $expense['user_id']; ?>&amp;month=<?php echo $expense['month']; ?>&amp;token=<?php echo $token; ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 16 16">
                                        <path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>
                                    </svg>
                                </a>
                                <a href="<?php echo $rootUrl;?>/expense-pay?user=<?php echo $expense['user_id']; ?>&amp;month=<?php echo $expense['month']; ?>&amp;token=<?php echo $token; ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-currency-euro" viewBox="0 0 16 16">
                                        <path d="M4 9.42h1.063C5.4 12.323 7.317 14 10.34 14c.622 0 1.167-.068 1.659-.185v-1.3c-.484.119-1.045.17-1.659.17-2.1 0-3.455-1.198-3.775-3.264h4.017v-.928H6.497v-.936c0-.11 0-.219.008-.329h4.078v-.927H6.618c.388-1.898 1.719-2.985 3.723-2.985.614 0 1.175.05 1.659.177V2.194A6.617 6.617 0 0 0 10.341 2c-2.928 0-4.82 1.569-5.244 4.3H4v.928h1.01v1.265H4v.928z"/>
                                    </svg>
                                </a>
                                <a href="<?php echo $rootUrl;?>/expense-refuse?user=<?php echo $expense['user_id']; ?>&amp;month=<?php echo $expense['month']; ?>&amp;token=<?php echo $token; ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-circle" viewBox="0 0 16 16">
                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                    </svg>
                                </a>
                            </td>   
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <br><br><br><br><br><br>
        </div>
    </body>
</html>
