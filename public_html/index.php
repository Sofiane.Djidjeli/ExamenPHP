<?php

require_once '../src/utils/Utils.php';
require_once '../src/utils/ConfigReader.php';
require_once '../src/controllers/DefaultController.php';
require_once '../src/controllers/ExpenseController.php';
require_once '../src/controllers/ExpenseProcessingController.php';

$requestInfo = getRequestInfo();

// Check.
if (! $requestInfo) {
    popup('stop');
    stop(); // Fin du script.
}


list(
    'httpMethod' => $httpMethod,
    'queryParams' => $queryParams,
    'path' => $path,
    'explodedPath' => $explodedPath
) = $requestInfo;

$GLOBALS['queryParams'] = $queryParams;


$path = $requestInfo['path'];
$fragments = explode("/", $path);
$control = array_shift($fragments);
$control = array_shift($fragments);
$control = array_shift($fragments);

switch ($_SERVER["REQUEST_METHOD"]) {
    case 'GET' : {
        switch ($control) {
            case '' :
            case 'index.php' :
            case 'user-showLogin' :
            {
                call_user_func_array([new UserController(), 'showLogin'], []);
                exit();
                break;
            }

            case 'expense-show' : {
                call_user_func_array([new ExpenseController(), 'show'], []);
                exit();
                break;
      
            }


            case 'expenseProcessing-show' : {
                call_user_func_array([new ExpenseProcessingController(), 'show'], []);
                exit();
                break;
            }
            case 'expense_line-remove' : {
                call_user_func_array([new ExpenseController(), 'removeLine'], []);
                exit();
                break;
            }
            case 'expense-approve': {
                call_user_func_array([new ExpenseProcessingController(), 'approveExpense'], []);
                exit();
                break;
            }
             case 'expense-pay': {
                call_user_func_array([new ExpenseProcessingController(), 'payExpense'], []);
                exit();
                break;
            } 
            case 'expense-refuse': {
                call_user_func_array([new ExpenseProcessingController(), 'refuseExpense'], []);
                exit();
                break;
            }
        }

        break;

    }
    

    case 'POST' : {
        switch ($control) {
            case 'user-login' : {
                call_user_func_array([new UserController(), 'login'], []);
                stop();
                break;
            }
                  
            case 'expense-update' : {
                call_user_func_array([new ExpenseController(), 'update'], []);
                exit();
                break;
            }
            
            case 'expense_line-create' : {
                call_user_func_array([new ExpenseController(), 'createLine'], []);
                exit();
                break;
            }
            
        }

        break;
    }

    default : {
        // Traitement par défaut.
        (new DefaultController())->show();
    }
}

?>